# Changelog

## v0.1.1 (21/06/2018)
- [**bug**] Fixed jumpy scrollbars.  [#9](https://github.com/octalmage/minum/issues/9)
- [**enhancement**] Use Electron.  [#7](https://github.com/octalmage/minum/issues/7)
- [**enhancement**] Use Chrome's webview instead of an iFrame. [#5](https://github.com/octalmage/minum/issues/5)
- [**bug**] Fix copy and paste.  [#11](https://github.com/octalmage/minum/issues/11)
